﻿namespace CommonData.Models
{
    public class Contact : EntityBase
    {
        public ContactType Type { get; set; }
        public string Content { get; set; }
        public User User { get; set; }
    }
}
