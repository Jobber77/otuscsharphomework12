﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonData.Models
{
    public class AdvertCategory : EntityBase
    {
        public string Name { get; set; }

        public IEnumerable<Advert> Adverts { get; set; }
    }
}
