﻿using System.Collections.Generic;

namespace CommonData.Models
{
    public class Advert : EntityBase
    {
        public string Title { get; set; }

        public string Text { get; set; }

        public bool Active { get; set; }

        public User User { get; set; }

        public IEnumerable<Item> Items { get; set; }

        public AdvertCategory AdvertCategory { get; set; }
    }
}
