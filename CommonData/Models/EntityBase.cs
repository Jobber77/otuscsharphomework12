﻿using System.ComponentModel.DataAnnotations;

namespace CommonData.Models
{
    public abstract class EntityBase
    {
        [Key]
        public int Id { get; set; }
    }
}
