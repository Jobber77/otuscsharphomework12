﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonData.Models
{
    public enum ContactType
    {
        Email = 0,
        Phone = 1,
        Telegram = 2,
        Viber = 3,
        Whatsapp = 4
    }
}
