﻿using System.Collections.Generic;

namespace CommonData.Models
{
    public class User : EntityBase
    {
        public string Login { get; set; }

        public string Name { get; set; }

        public IEnumerable<Advert> Adverts { get; set; }

        public IEnumerable<Contact> Contacts { get; set; }
    }
}
