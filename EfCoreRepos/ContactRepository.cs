﻿using CommonData.Interfaces;
using CommonData.Models;
using EfCoreRepos.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EfCoreRepos
{
    public class ContactRepository : IRepository<Contact>
    {
        private readonly AvitoDbContext _context;

        public ContactRepository(AvitoDbContext context)
        {
            _context = context;
        }
        public async Task<Contact> GetAsync(int id) => await _context.Contacts.FindAsync(id);

        public async Task<IEnumerable<Contact>> GetAllAsync() => await _context.Contacts.ToListAsync();

        public async Task<Contact> CreateAsync(Contact entity)
        {
            _context.Contacts.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<Contact> UpdateAsync(Contact entity)
        {
            _context.Contacts.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            _context.Contacts.Remove(new Contact() { Id = id });
            await _context.SaveChangesAsync();
        }
    }
}
