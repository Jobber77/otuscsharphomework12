﻿using CommonData.Models;
using Microsoft.EntityFrameworkCore;

namespace EfCoreRepos.DatabaseContext
{
    public class AvitoDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Advert> Adverts { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<AdvertCategory> AdvertCategories { get; set; }

        public AvitoDbContext(DbContextOptions<AvitoDbContext> options) : base (options)
        {
        }
    }
}
