﻿using CommonData.Interfaces;
using CommonData.Models;
using EfCoreRepos;
using EfCoreRepos.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Repos.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterRepositoryServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AvitoDbContext>(options =>
                options.UseSqlServer(configuration["ConnectionStrings:DefaultConnection"]));

            services.TryAddTransient<IRepository<User>, UserRepository>();
            services.TryAddTransient<IRepository<Advert>, AdvertRepository>();
            services.TryAddTransient<IRepository<AdvertCategory>, AdvertCategoryRepository>();
            services.TryAddTransient<IRepository<Contact>, ContactRepository>();
            services.TryAddTransient<IRepository<Item>, ItemRepository>();
        }
    }
}
