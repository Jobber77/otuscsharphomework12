﻿using CommonData.Interfaces;
using CommonData.Models;
using EfCoreRepos.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repos
{
    public class UserRepository : IRepository<User>
    {
        private readonly AvitoDbContext _context;

        public UserRepository(AvitoDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetAsync(int id) => await _context.Users.FindAsync(id);

        public async Task<IEnumerable<User>> GetAllAsync() => await _context.Users.ToListAsync();


        public async Task<User> CreateAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User> UpdateAsync(User user)
        {
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task DeleteAsync(int id)
        {
            _context.Users.Remove(new User {Id = id});
            await _context.SaveChangesAsync();
        }
    }
}
