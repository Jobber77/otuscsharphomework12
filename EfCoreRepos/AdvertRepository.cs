﻿using CommonData.Interfaces;
using CommonData.Models;
using EfCoreRepos.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EfCoreRepos
{
    public class AdvertRepository : IRepository<Advert>
    {
        private readonly AvitoDbContext _context;

        public AdvertRepository(AvitoDbContext context)
        {
            _context = context;
        }
        public async Task<Advert> GetAsync(int id) => await _context.Adverts.FindAsync(id);

        public async Task<IEnumerable<Advert>> GetAllAsync() => await _context.Adverts.ToListAsync();

        public async Task<Advert> CreateAsync(Advert entity)
        {
            _context.Adverts.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<Advert> UpdateAsync(Advert entity)
        {
            _context.Adverts.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            _context.Adverts.Remove(new Advert() { Id = id });
            await _context.SaveChangesAsync();
        }
    }
}
