﻿using CommonData.Interfaces;
using CommonData.Models;
using EfCoreRepos.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repos
{
    public class AdvertCategoryRepository : IRepository<AdvertCategory>
    {
        private readonly AvitoDbContext _context;

        public AdvertCategoryRepository(AvitoDbContext context)
        {
            _context = context;
        }

        public async Task<AdvertCategory> GetAsync(int id) => await _context.AdvertCategories.FindAsync(id);

        public async Task<IEnumerable<AdvertCategory>> GetAllAsync() => await _context.AdvertCategories.ToListAsync();


        public async Task<AdvertCategory> CreateAsync(AdvertCategory entity)
        {
            _context.AdvertCategories.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<AdvertCategory> UpdateAsync(AdvertCategory entity)
        {
            _context.AdvertCategories.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            _context.AdvertCategories.Remove(new AdvertCategory {Id = id});
            await _context.SaveChangesAsync();
        }
    }
}
