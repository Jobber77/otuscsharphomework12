﻿using CommonData.Interfaces;
using CommonData.Models;
using EfCoreRepos.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EfCoreRepos
{
    public class ItemRepository : IRepository<Item>
    {
        private readonly AvitoDbContext _context;

        public ItemRepository(AvitoDbContext context)
        {
            _context = context;
        }
        public async Task<Item> GetAsync(int id) => await _context.Items.FindAsync(id);

        public async Task<IEnumerable<Item>> GetAllAsync() => await _context.Items.ToListAsync();

        public async Task<Item> CreateAsync(Item entity)
        {
            _context.Items.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<Item> UpdateAsync(Item entity)
        {
            _context.Items.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            _context.Items.Remove(new Item() { Id = id });
            await _context.SaveChangesAsync();
        }
    }
}
