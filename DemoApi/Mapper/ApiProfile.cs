﻿using AutoMapper;
using CommonData.Models;
using DemoApi.ViewModels;

namespace DemoApi.Mapper
{
    public class ApiProfile : Profile
    {
        public ApiProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<UserViewModel, User>();
            CreateMap<Advert, AdvertViewModel>();
            CreateMap<AdvertViewModel, Advert>();
            CreateMap<AdvertCategory, AdvertCategoryViewModel>();
            CreateMap<AdvertCategoryViewModel, AdvertCategory>();
            CreateMap<Contact, ContactViewModel>();
            CreateMap<ContactViewModel, Contact>();
            CreateMap<Item, ItemViewModel>();
            CreateMap<ItemViewModel, Item>();
        }
    }
}
