﻿using CommonData.Interfaces;
using CommonData.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DemoApi.ViewModels;

namespace DemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IRepository<User> _repo;
        private IMapper _mapper;

        public UserController(IRepository<User> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        // GET: api/User
        [HttpGet]
        public async Task<IEnumerable<UserViewModel>> Get() => _mapper.Map<IEnumerable<UserViewModel>>(await _repo.GetAllAsync());

        // GET: api/User/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<UserViewModel> Get(int id) => _mapper.Map<UserViewModel>(await _repo.GetAsync(id));

        // POST: api/User
        [HttpPost]
        public async Task<UserViewModel> Post([FromBody] UserViewModel userViewModel)
        {
            var user = _mapper.Map<User>(userViewModel);
            var result = await _repo.CreateAsync(user);
            return _mapper.Map<UserViewModel>(result);
        }

        // DELETE: api/User/5
        [HttpPut]
        public async Task<UserViewModel> Put(UserViewModel userViewModel)
        {
            var user = _mapper.Map<User>(userViewModel);
            var result = await _repo.UpdateAsync(user);
            return _mapper.Map<UserViewModel>(result);
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.DeleteAsync(id);
        }
    }
}
