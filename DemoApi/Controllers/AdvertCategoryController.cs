﻿using CommonData.Interfaces;
using CommonData.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DemoApi.ViewModels;

namespace DemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdvertCategoryController : ControllerBase
    {
        private IRepository<AdvertCategory> _repo;
        private IMapper _mapper;

        public AdvertCategoryController(IRepository<AdvertCategory> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        // GET: api/AdvertCategory
        [HttpGet]
        public async Task<IEnumerable<AdvertCategoryViewModel>> Get() => _mapper.Map<IEnumerable<AdvertCategoryViewModel>>(await _repo.GetAllAsync());

        // GET: api/AdvertCategory/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<AdvertCategoryViewModel> Get(int id) => _mapper.Map<AdvertCategoryViewModel>(await _repo.GetAsync(id));

        // POST: api/AdvertCategory
        [HttpPost]
        public async Task<AdvertCategoryViewModel> Post([FromBody] AdvertCategoryViewModel entityViewModel)
        {
            var entity = _mapper.Map<AdvertCategory>(entityViewModel);
            var result = await _repo.CreateAsync(entity);
            return _mapper.Map<AdvertCategoryViewModel>(result);
        }

        // DELETE: api/AdvertCategory/5
        [HttpPut]
        public async Task<AdvertCategoryViewModel> Put(AdvertCategoryViewModel entityViewModel)
        {
            var entity = _mapper.Map<AdvertCategory>(entityViewModel);
            var result = await _repo.UpdateAsync(entity);
            return _mapper.Map<AdvertCategoryViewModel>(result);
        }

        // DELETE: api/AdvertCategory/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.DeleteAsync(id);
        }
    }
}
