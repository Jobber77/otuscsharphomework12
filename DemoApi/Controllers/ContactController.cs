﻿using CommonData.Interfaces;
using CommonData.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DemoApi.ViewModels;
using System.Linq;

namespace DemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private IRepository<Contact> _repo;
        private IRepository<Contact> _userRepo;
        private IMapper _mapper;

        public ContactController(IRepository<Contact> repo, IRepository<Contact> userRepo, IMapper mapper)
        {
            _repo = repo;
            _userRepo = userRepo;
            _mapper = mapper;
        }
        // GET: api/Contact
        [HttpGet]
        public async Task<IEnumerable<ContactViewModel>> Get() => _mapper.Map<IEnumerable<ContactViewModel>>(await _repo.GetAllAsync());

        // GET: api/Contact/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ContactViewModel> Get(int id) => _mapper.Map<ContactViewModel>(await _repo.GetAsync(id));

        // POST: api/Contact
        [HttpPost]
        public async Task<ContactViewModel> Post([FromBody] ContactViewModel entityViewModel)
        {
            var entity = _mapper.Map<Contact>(entityViewModel);
            var result = await _repo.CreateAsync(entity);
            return _mapper.Map<ContactViewModel>(result);
        }

        // DELETE: api/Contact/5
        [HttpPut]
        public async Task<ContactViewModel> Put(ContactViewModel entityViewModel)
        {
            var entity = _mapper.Map<Contact>(entityViewModel);
            var result = await _repo.UpdateAsync(entity);
            return _mapper.Map<ContactViewModel>(result);
        }

        // DELETE: api/Contact/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.DeleteAsync(id);
        }

        [HttpGet]
        public async Task<IEnumerable<ContactViewModel>> GetUserContacts(int userId)
        {
            var user = await _userRepo.GetAsync(userId);
            var items = await _repo.GetAllAsync();
            var result = items.Where(item => item.User.Id == user.Id);
            return _mapper.Map<IEnumerable<ContactViewModel>>(result);
        }

        [HttpGet]
        public async Task<IEnumerable<ContactViewModel>> GetUserContactsByType(int userId, ContactType type)
        {
            var user = await _userRepo.GetAsync(userId);
            var items = await _repo.GetAllAsync();
            var result = items.Where(item => item.User.Id == user.Id).Where(item => item.Type == type);
            return _mapper.Map<IEnumerable<ContactViewModel>>(result);
        }
    }
}
