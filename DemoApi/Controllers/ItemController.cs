﻿using CommonData.Interfaces;
using CommonData.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DemoApi.ViewModels;
using System.Linq;

namespace DemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private IRepository<Item> _repo;
        private IRepository<Advert> _advertRepo;
        private IMapper _mapper;

        public ItemController(IRepository<Item> repo, IRepository<Advert> advertRepo, IMapper mapper)
        {
            _repo = repo;
            _advertRepo = advertRepo;
            _mapper = mapper;
        }
        // GET: api/Item
        [HttpGet]
        public async Task<IEnumerable<ItemViewModel>> Get() => _mapper.Map<IEnumerable<ItemViewModel>>(await _repo.GetAllAsync());

        // GET: api/Item/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ItemViewModel> Get(int id) => _mapper.Map<ItemViewModel>(await _repo.GetAsync(id));

        // POST: api/Item
        [HttpPost]
        public async Task<ItemViewModel> Post([FromBody] ItemViewModel entityViewModel)
        {
            var entity = _mapper.Map<Item>(entityViewModel);
            var result = await _repo.CreateAsync(entity);
            return _mapper.Map<ItemViewModel>(result);
        }

        // DELETE: api/Item/5
        [HttpPut]
        public async Task<ItemViewModel> Put(ItemViewModel entityViewModel)
        {
            var entity = _mapper.Map<Item>(entityViewModel);
            var result = await _repo.UpdateAsync(entity);
            return _mapper.Map<ItemViewModel>(result);
        }

        // DELETE: api/Item/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.DeleteAsync(id);
        }

        [HttpGet]
        public async Task<IEnumerable<ItemViewModel>> GetItemsByAdvert(int advertId)
        {
            var advert = await _advertRepo.GetAsync(advertId);
            var items = await _repo.GetAllAsync();
            return _mapper.Map<IEnumerable<ItemViewModel>>(items.Where(item => item.Advert.Id == advert.Id));
        }

        [HttpGet]
        public async Task<IEnumerable<ItemViewModel>> GetItemsByPrice(decimal minPrice, decimal maxPrice)
        {
            var items = await _repo.GetAllAsync();
            return _mapper.Map<IEnumerable<ItemViewModel>>(items.Where(item => item.Price > minPrice && item.Price > maxPrice));
        }
    }
}
