﻿using CommonData.Interfaces;
using CommonData.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DemoApi.ViewModels;

namespace DemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdvertController : ControllerBase
    {
        private IRepository<Advert> _repo;
        private IMapper _mapper;

        public AdvertController(IRepository<Advert> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        // GET: api/Advert
        [HttpGet]
        public async Task<IEnumerable<AdvertViewModel>> Get() => _mapper.Map<IEnumerable<AdvertViewModel>>(await _repo.GetAllAsync());

        // GET: api/Advert/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<AdvertViewModel> Get(int id) => _mapper.Map<AdvertViewModel>(await _repo.GetAsync(id));

        // POST: api/Advert
        [HttpPost]
        public async Task<AdvertViewModel> Post([FromBody] AdvertViewModel entityViewModel)
        {
            var entity = _mapper.Map<Advert>(entityViewModel);
            var result = await _repo.CreateAsync(entity);
            return _mapper.Map<AdvertViewModel>(result);
        }

        // DELETE: api/Advert/5
        [HttpPut]
        public async Task<AdvertViewModel> Put(AdvertViewModel entityViewModel)
        {
            var entity = _mapper.Map<Advert>(entityViewModel);
            var result = await _repo.UpdateAsync(entity);
            return _mapper.Map<AdvertViewModel>(result);
        }

        // DELETE: api/Advert/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.DeleteAsync(id);
        }
    }
}
