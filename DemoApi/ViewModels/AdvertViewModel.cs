﻿using System.Collections.Generic;
using CommonData.Models;

namespace DemoApi.ViewModels
{
    public class AdvertViewModel : ViewModelBase
    {
        public string Title { get; set; }

        public string Text { get; set; }

        public bool Active { get; set; }

        public UserViewModel User { get; set; }

        public IEnumerable<ItemViewModel> Items { get; set; }

        public AdvertCategoryViewModel AdvertCategory { get; set; }
    }
}
