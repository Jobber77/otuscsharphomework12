﻿using System.Collections.Generic;

namespace DemoApi.ViewModels
{
    public class AdvertCategoryViewModel : ViewModelBase
    {
        public string Name { get; set; }

        public IEnumerable<AdvertViewModel> Adverts { get; set; }
    }
}
