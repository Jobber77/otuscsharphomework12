﻿using CommonData.Models;

namespace DemoApi.ViewModels
{
    public class ItemViewModel : ViewModelBase
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public AdvertViewModel Advert { get; set; }
    }
}
