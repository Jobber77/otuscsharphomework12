﻿using System.Collections.Generic;
using CommonData.Models;

namespace DemoApi.ViewModels
{
    public class UserViewModel : ViewModelBase
    {
        public string Login { get; set; }

        public string Name { get; set; }

        public IEnumerable<AdvertViewModel> Adverts { get; set; }

        public IEnumerable<ContactViewModel> Contacts { get; set; }
    }
}
