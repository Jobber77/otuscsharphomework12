﻿using CommonData.Models;

namespace DemoApi.ViewModels
{
    public class ContactViewModel : ViewModelBase
    {
        public ContactType Type { get; set; }
        public string Content { get; set; }
        public UserViewModel User { get; set; }
    }
}
