﻿CREATE PROCEDURE [dbo].[UpdateItem]
	@Id INT = null,
	@Name NVARCHAR(MAX) = null,
	@Price DECIMAL(18,2) = null,
	@AdvertId INT = null
AS
BEGIN
	UPDATE [dbo].[Items]
	SET 
		[Name] = @Name,
		[Price] = @Price,
		[AdvertId] = @AdvertId
	WHERE [Id] = @Id

	SELECT
		[Id],
		[Name],
		[Price],
		[AdvertId]
	FROM [dbo].[Items] WHERE [Id] = @Id
END