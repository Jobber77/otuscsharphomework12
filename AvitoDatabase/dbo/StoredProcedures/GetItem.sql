﻿CREATE PROCEDURE [dbo].[GetItem]
	@Id INT = null
AS
BEGIN
	SELECT TOP 1
		[Id],
		[Name],
		[Price],
		[AdvertId]
	FROM [dbo].[Items] WHERE [Id] = @Id
END
