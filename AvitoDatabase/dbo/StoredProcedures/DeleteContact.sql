﻿CREATE PROCEDURE [dbo].[DeleteContact]
	@Id INT = null
AS
BEGIN
	DELETE [dbo].[Contact]
	WHERE [Id] = @Id
END
