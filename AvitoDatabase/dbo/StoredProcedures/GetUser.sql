﻿CREATE PROCEDURE [dbo].[GetUser]
	@Id INT = null
AS
BEGIN
	SELECT TOP 1 
		[Id], 
		[Login], 
		[Name] 
	FROM [dbo].[Users] 
	WHERE [Id] = @Id;
END
