﻿CREATE PROCEDURE [dbo].[SelectAdvert]
AS
BEGIN
	SELECT
		[Id],
		[Title],
		[Text],
		[UserId],
		[AdvertCategoryId]
	FROM [dbo].[Adverts] 
END
