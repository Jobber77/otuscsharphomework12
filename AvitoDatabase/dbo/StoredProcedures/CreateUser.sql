﻿CREATE PROCEDURE [dbo].[CreateUser]
	@Login NVARCHAR(MAX) = null,
	@Name NVARCHAR(MAX) = null
AS
BEGIN
	INSERT INTO [dbo].[Users] 
	(
		[Login], 
		[Name] 
	)	
	VALUES
	(
		@Login,
		@Name
	)

	DECLARE @Id INT = SCOPE_IDENTITY()

	SELECT
		[Id],
		[Login], 
		[Name]
	FROM [dbo].[Users] WHERE [Id] = @Id
END
