﻿CREATE PROCEDURE [dbo].[SelectContact]
AS
BEGIN
	SELECT
		[Id], 
		[Type],
		[Content]
	FROM [dbo].[Contact] 
END