﻿CREATE PROCEDURE [dbo].[DeleteUser]
	@Id INT = null
AS
BEGIN
	DELETE [dbo].[Users]
	WHERE [Id] = @Id
END
