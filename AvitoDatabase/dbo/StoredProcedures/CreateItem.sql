﻿CREATE PROCEDURE [dbo].[CreateItem]
	@Name NVARCHAR(MAX) = null,
	@Price DECIMAL(18,2) = null,
	@AdvertId INT = null
AS
BEGIN
	INSERT INTO [dbo].[Items] 
	(
		[Name],
		[Price],
		[AdvertId]
	)	
	VALUES
	(
		@Name,
		@Price,
		@AdvertId
	)

	DECLARE @Id INT = SCOPE_IDENTITY()

	SELECT
		[Name],
		[Price],
		[AdvertId]
	FROM [dbo].[Items] WHERE [Id] = @Id
END

