﻿CREATE PROCEDURE [dbo].[DeleteAdvertCategory]
	@Id INT = null
AS
BEGIN
	DELETE [dbo].[AdvertCategory]
	WHERE [Id] = @Id
END

