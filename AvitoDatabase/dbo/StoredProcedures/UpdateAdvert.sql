﻿CREATE PROCEDURE [dbo].[UpdateAdvert]
	@Id INT = null,
	@Title NVARCHAR(MAX) = null,
	@Text NVARCHAR(MAX) = null,
	@UserId INT = null,
	@AdvertCategoryId INT = null
AS
BEGIN
	UPDATE [dbo].[Adverts]
	SET 
		[Title] = @Title,
		[Text] = @Text,
		[UserId] = @UserId,
		[AdvertCategoryId] = @AdvertCategoryId
	WHERE [Id] = @Id

	SELECT
		[Id],
		[Login], 
		[Name]
	FROM [dbo].[Users] WHERE [Id] = @Id
END
