﻿CREATE PROCEDURE [dbo].[UpdateUser]
	@Id INT = null,
	@Login NVARCHAR(MAX) = null,
	@Name NVARCHAR(MAX) = null
AS
BEGIN
	UPDATE [dbo].[Users]
	SET 
		[Login] = @Login,
		[Name] = @Name
	WHERE [Id] = @Id

	SELECT
		[Id],
		[Login], 
		[Name]
	FROM [dbo].[Users] WHERE [Id] = @Id
END