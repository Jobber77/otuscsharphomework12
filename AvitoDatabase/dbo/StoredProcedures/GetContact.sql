﻿CREATE PROCEDURE [dbo].[GetContact]
	@Id INT = null
AS
BEGIN
	SELECT TOP 1 
		[Id], 
		[Type],
		[Content]
	FROM [dbo].[Contact] 
	WHERE [Id] = @Id;
END
