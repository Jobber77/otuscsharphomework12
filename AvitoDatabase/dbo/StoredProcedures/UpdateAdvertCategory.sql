﻿CREATE PROCEDURE [dbo].[UpdateAdvertCategory]
	@Id INT = null,
	@Name NVARCHAR(MAX) = null
AS
BEGIN
	UPDATE [dbo].[AdvertCategory]
	SET 
		[Name] = @Name
	WHERE [Id] = @Id

	SELECT
		[Id],
		[Name]
	FROM [dbo].[AdvertCategory] WHERE [Id] = @Id
END
