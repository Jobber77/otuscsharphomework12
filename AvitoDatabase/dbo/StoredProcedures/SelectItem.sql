﻿CREATE PROCEDURE [dbo].[SelectItem]
AS
BEGIN
	SELECT
		[Id],
		[Name],
		[Price],
		[AdvertId]
	FROM [dbo].[Items]
END
