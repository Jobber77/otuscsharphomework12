﻿CREATE PROCEDURE [dbo].[CreateAdvertCategory]
	@Name NVARCHAR(MAX) = null
AS
BEGIN
	INSERT INTO [dbo].[AdvertCategory] 
	(
		[Name] 
	)	
	VALUES
	(
		@Name
	)

	DECLARE @Id INT = SCOPE_IDENTITY()

	SELECT
		[Id],
		[Name]
	FROM [dbo].[AdvertCategory] WHERE [Id] = @Id
END

