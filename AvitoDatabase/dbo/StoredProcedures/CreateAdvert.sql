﻿CREATE PROCEDURE [dbo].[CreateAdvert]
	@Title NVARCHAR(MAX) = null,
	@Text NVARCHAR(MAX) = null,
	@UserId INT = null,
	@AdvertCategoryId INT = null
AS
BEGIN
	INSERT INTO [dbo].[Adverts] 
	(
		[Title],
		[Text],
		[UserId],
		[AdvertCategoryId]
	)	
	VALUES
	(
		@Title,
		@Text,
		@UserId,
		@AdvertCategoryId
	)

	DECLARE @Id INT = SCOPE_IDENTITY()

	SELECT
		[Id],
		[Title],
		[Text],
		[UserId],
		[AdvertCategoryId]
	FROM [dbo].[Adverts] WHERE [Id] = @Id
END

