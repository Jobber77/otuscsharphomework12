﻿CREATE PROCEDURE [dbo].[UpdateContact]
	@Id INT = null,
	@Type INT = null, 
	@Content NVARCHAR(MAX) = null,
	@UserId INT = null
AS
BEGIN
	UPDATE [dbo].[Contact]
	SET 
		[Type] = @Type,
		[Content] = @Content,
		[UserId] = @UserId
	WHERE [Id] = @Id

	SELECT
		[Id],
		[Type],
		[Content],
		[UserId]
	FROM [dbo].[Contact] WHERE [Id] = @Id
END

