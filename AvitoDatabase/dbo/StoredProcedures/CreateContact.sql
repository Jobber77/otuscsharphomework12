﻿CREATE PROCEDURE [dbo].[CreateContact]
	@Type INT = null, 
	@Content NVARCHAR(MAX) = null,
	@UserId INT = null
AS
BEGIN
	INSERT INTO [dbo].[Contact] 
	(
		[Type], 
		[Content],
		[UserId]
	)	
	VALUES
	(
		@Type,
		@Content,
		@UserId
	)

	DECLARE @Id INT = SCOPE_IDENTITY()

	SELECT
		[Type], 
		[Content],
		[UserId]
	FROM [dbo].[Contact] WHERE [Id] = @Id
END
