﻿CREATE PROCEDURE [dbo].[GetAdvertCategory]
	@Id INT = null
AS
BEGIN
	SELECT TOP 1 
		[Id], 
		[Name] 
	FROM [dbo].[AdvertCategory] 
	WHERE [Id] = @Id;
END
