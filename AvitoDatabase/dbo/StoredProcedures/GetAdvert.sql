﻿CREATE PROCEDURE [dbo].[GetAdvert]
	@Id INT = null
AS
BEGIN
	SELECT TOP 1
		[Id],
		[Title],
		[Text],
		[UserId],
		[AdvertCategoryId]
	FROM [dbo].[Adverts] WHERE [Id] = @Id
END