﻿CREATE TABLE [dbo].[Items] (
    [Id]       INT             IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (MAX)  NULL,
    [Price]    DECIMAL (18, 2) NOT NULL,
    [AdvertId] INT             NULL,
    CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Items_Adverts_AdvertId] FOREIGN KEY ([AdvertId]) REFERENCES [dbo].[Adverts] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Items_AdvertId]
    ON [dbo].[Items]([AdvertId] ASC);

