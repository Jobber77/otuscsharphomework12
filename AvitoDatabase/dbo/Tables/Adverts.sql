﻿CREATE TABLE [dbo].[Adverts] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Title]            NVARCHAR (MAX) NULL,
    [Text]             NVARCHAR (MAX) NULL,
    [Active]           BIT            NOT NULL,
    [UserId]           INT            NULL,
    [AdvertCategoryId] INT            NULL,
    CONSTRAINT [PK_Adverts] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Adverts_AdvertCategory_AdvertCategoryId] FOREIGN KEY ([AdvertCategoryId]) REFERENCES [dbo].[AdvertCategory] ([Id]),
    CONSTRAINT [FK_Adverts_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Adverts_AdvertCategoryId]
    ON [dbo].[Adverts]([AdvertCategoryId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Adverts_UserId]
    ON [dbo].[Adverts]([UserId] ASC);

