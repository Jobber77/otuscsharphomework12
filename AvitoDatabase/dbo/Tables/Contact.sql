﻿CREATE TABLE [dbo].[Contact] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Type]    INT            NOT NULL,
    [Content] NVARCHAR (MAX) NULL,
    [UserId]  INT            NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Contact_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Contact_UserId]
    ON [dbo].[Contact]([UserId] ASC);

