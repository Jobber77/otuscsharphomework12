﻿CREATE TABLE [dbo].[AdvertCategory] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AdvertCategory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

