﻿using AdoRepos.Base;
using CommonData.Interfaces;
using CommonData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repos
{
    public class ItemRepository : BaseRepo<Item>, IRepository<Item>
    {
        public ItemRepository(string conncetionString) : base(conncetionString)
        {
            EntityName = "Item";
        }

        public async Task<Item> GetAsync(int id) => await ExecuteNonQuery(GetCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        public async Task<IEnumerable<Item>> GetAllAsync() => await ExecuteNonQueryEnumerable(SelectAllCommand);

        public async Task<Item> CreateAsync(Item entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Name", entity.Name, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Price", entity.Price, SqlDbType.Decimal, ParameterDirection.Input));
            parameters.Add(CreateParameter("@AdvertId", entity.Advert.Id, SqlDbType.Int, ParameterDirection.Input));
            return await ExecuteNonQuery(CreateCommand, parameters.ToArray());
        }

        public async Task<Item> UpdateAsync(Item entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Name", entity.Name, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Price", entity.Price, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@AdvertId", entity.Advert.Id, SqlDbType.Int, ParameterDirection.Input));
            return await ExecuteNonQuery(UpdateCommand, parameters.ToArray());
        }

        public async Task DeleteAsync(int id) => await ExecuteNonQuery(DeleteCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        protected override void FillFromDataReader(Item entity, IDataReader dataReader)
        {
            base.FillFromDataReader(entity, dataReader);
            entity.Name = dataReader[1] is DBNull ? null : Convert.ToString(dataReader[1]);
            entity.Price = dataReader[2] is DBNull ? default : Convert.ToDecimal(dataReader[2]);
            entity.Advert.Id = dataReader[3] is DBNull ? default : Convert.ToInt32(dataReader[3]);
        }
    }
}
