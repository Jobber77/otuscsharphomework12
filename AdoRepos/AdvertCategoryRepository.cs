﻿using AdoRepos.Base;
using CommonData.Interfaces;
using CommonData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repos
{
    public class AdvertCategoryRepository : BaseRepo<AdvertCategory>, IRepository<AdvertCategory>
    {
        public AdvertCategoryRepository(string conncetionString) : base(conncetionString)
        {
            EntityName = "AdvertCategory";
        }

        public async Task<AdvertCategory> GetAsync(int id) => await ExecuteNonQuery(GetCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));


        public async Task<IEnumerable<AdvertCategory>> GetAllAsync() => await ExecuteNonQueryEnumerable(SelectAllCommand);

        public async Task<AdvertCategory> CreateAsync(AdvertCategory entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Name", entity.Name, SqlDbType.NVarChar, ParameterDirection.Input));
            return await ExecuteNonQuery(CreateCommand, parameters.ToArray());
        }

        public async Task<AdvertCategory> UpdateAsync(AdvertCategory entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Id", entity.Id, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Name", entity.Name, SqlDbType.NVarChar, ParameterDirection.Input));
            return await ExecuteNonQuery(UpdateCommand, parameters.ToArray());
        }

        public async Task DeleteAsync(int id) => await ExecuteNonQuery(DeleteCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        protected override void FillFromDataReader(AdvertCategory entity, IDataReader dataReader)
        {
            base.FillFromDataReader(entity, dataReader);
            entity.Name = dataReader[1] is DBNull ? null : Convert.ToString(dataReader[1]);
        }
    }
}
