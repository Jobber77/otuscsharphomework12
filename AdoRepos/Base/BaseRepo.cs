﻿using CommonData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AdoRepos.Base
{
    public abstract class BaseRepo<T> where T: EntityBase, new()
    {
        private string _conncetionString;
        public string EntityName { get; set; }
        public string SelectAllCommand => $"Select{EntityName}";
        public string GetCommand => $"Get{EntityName}";
        public string CreateCommand => $"Create{EntityName}";
        public string UpdateCommand => $"Update{EntityName}";
        public string DeleteCommand => $"Delete{EntityName}";

        public BaseRepo(string conncetionString)
        {
            _conncetionString = conncetionString;
        }

        protected virtual async Task<T> ExecuteNonQuery(string storedProcedureName, params SqlParameter[] parameters)
        {
            if (string.IsNullOrEmpty(storedProcedureName))
                throw new ArgumentNullException(nameof(storedProcedureName));

            T result = default;

            using (SqlConnection connection = new SqlConnection(_conncetionString))
            using (SqlCommand command = new SqlCommand($"[dbo].[{storedProcedureName}]", connection))
            {
                FillParameters(parameters, command);
                command.CommandType = CommandType.StoredProcedure;
                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    if (await reader.ReadAsync())
                    {
                        result = new T();
                        FillFromDataReader(result, reader);
                    }
                }
            }

            return result;
        }

        protected virtual async Task<IEnumerable<T>> ExecuteNonQueryEnumerable(string storedProcedureName, params SqlParameter[] parameters)
        {
            if (string.IsNullOrEmpty(storedProcedureName))
                throw new ArgumentNullException(nameof(storedProcedureName));

            List<T> result = new List<T>();

            using (SqlConnection connection = new SqlConnection(_conncetionString))
            using (SqlCommand command = new SqlCommand($"[dbo].[{storedProcedureName}]", connection))
            {
                FillParameters(parameters, command);
                command.CommandType = CommandType.StoredProcedure;
                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var entity = new T();
                        FillFromDataReader(entity, reader);
                        result.Add(entity);
                    }
                }
            }

            return result.Any() ? result : null;
        }

        protected virtual void FillFromDataReader(T user, IDataReader dataReader)
        {
            user.Id = dataReader[0] is DBNull ? 0 : Convert.ToInt32(dataReader[0]);
        }

        protected virtual SqlParameter CreateParameter(string name, object value, SqlDbType type, ParameterDirection direction) 
            => new SqlParameter(name, type) { Value = value , Direction = direction };

        protected virtual void FillParameters(IEnumerable<SqlParameter> parameters, IDbCommand command)
        {
            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
        }
    }
}
