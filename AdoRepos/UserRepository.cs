﻿using AdoRepos.Base;
using CommonData.Interfaces;
using CommonData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repos
{
    public class UserRepository : BaseRepo<User>, IRepository<User>
    {
        public UserRepository(string conncetionString) : base(conncetionString)
        {
            EntityName = "User";
        }

        public async Task<User> GetAsync(int id) => await ExecuteNonQuery(GetCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));


        public async Task<IEnumerable<User>> GetAllAsync() => await ExecuteNonQueryEnumerable(SelectAllCommand);

        public async Task<User> CreateAsync(User user)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Login", user.Login, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Name", user.Name, SqlDbType.NVarChar, ParameterDirection.Input));
            return await ExecuteNonQuery(CreateCommand, parameters.ToArray());
        }

        public async Task<User> UpdateAsync(User user)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Id", user.Id, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Login", user.Login, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Name", user.Name, SqlDbType.NVarChar, ParameterDirection.Input));
            return await ExecuteNonQuery(UpdateCommand, parameters.ToArray());
        }

        public async Task DeleteAsync(int id) => await ExecuteNonQuery(DeleteCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        protected override void FillFromDataReader(User user, IDataReader dataReader)
        {
            base.FillFromDataReader(user, dataReader);
            user.Login = dataReader[1] is DBNull ? null : Convert.ToString(dataReader[1]);
            user.Name = dataReader[2] is DBNull ? null : Convert.ToString(dataReader[2]);
        }
    }
}
