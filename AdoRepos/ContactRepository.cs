﻿using AdoRepos.Base;
using CommonData.Interfaces;
using CommonData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repos
{
    public class ContactRepository : BaseRepo<Contact>, IRepository<Contact>
    {
        public ContactRepository(string conncetionString) : base(conncetionString)
        {
            EntityName = "Contact";
        }

        public async Task<Contact> GetAsync(int id) => await ExecuteNonQuery(GetCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        public async Task<IEnumerable<Contact>> GetAllAsync() => await ExecuteNonQueryEnumerable(SelectAllCommand);

        public async Task<Contact> CreateAsync(Contact entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Type", entity.Type, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Content", entity.Content, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@UserId", entity.User?.Id, SqlDbType.Int, ParameterDirection.Input));
            return await ExecuteNonQuery(CreateCommand, parameters.ToArray());
        }

        public async Task<Contact> UpdateAsync(Contact entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Type", entity.Type, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Content", entity.Content, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@UserId", entity.User?.Id, SqlDbType.Int, ParameterDirection.Input));
            return await ExecuteNonQuery(UpdateCommand, parameters.ToArray());
        }

        public async Task DeleteAsync(int id) => await ExecuteNonQuery(DeleteCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        protected override void FillFromDataReader(Contact entity, IDataReader dataReader)
        {
            base.FillFromDataReader(entity, dataReader);
            entity.Type = dataReader[1] is DBNull ? default : (ContactType)Convert.ToInt32(dataReader[1]);
            entity.Content = dataReader[2] is DBNull ? null : Convert.ToString(dataReader[2]);
            entity.User.Id = dataReader[3] is DBNull ? 0 : Convert.ToInt32(dataReader[3]);
        }
    }
}
