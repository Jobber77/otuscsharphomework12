﻿using AdoRepos.Base;
using CommonData.Interfaces;
using CommonData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repos
{
    public class AdvertRepository : BaseRepo<Advert>, IRepository<Advert>
    {
        public AdvertRepository(string conncetionString) : base(conncetionString)
        {
            EntityName = "Advert";
        }

        public async Task<Advert> GetAsync(int id) => await ExecuteNonQuery(GetCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));


        public async Task<IEnumerable<Advert>> GetAllAsync() => await ExecuteNonQueryEnumerable(SelectAllCommand);

        public async Task<Advert> CreateAsync(Advert entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Title", entity.Title, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Text", entity.Text, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@UserId", entity.User?.Id, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@AdvertCategoryId", entity.AdvertCategory?.Id, SqlDbType.Int, ParameterDirection.Input));
            return await ExecuteNonQuery(CreateCommand, parameters.ToArray());
        }

        public async Task<Advert> UpdateAsync(Advert entity)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(CreateParameter("@Title", entity.Title, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@Text", entity.Text, SqlDbType.NVarChar, ParameterDirection.Input));
            parameters.Add(CreateParameter("@UserId", entity.User?.Id, SqlDbType.Int, ParameterDirection.Input));
            parameters.Add(CreateParameter("@AdvertCategory", entity.AdvertCategory?.Id, SqlDbType.Int, ParameterDirection.Input));
            return await ExecuteNonQuery(UpdateCommand, parameters.ToArray());
        }

        public async Task DeleteAsync(int id) => await ExecuteNonQuery(DeleteCommand, CreateParameter("@Id", id, SqlDbType.Int, ParameterDirection.Input));

        protected override void FillFromDataReader(Advert entity, IDataReader dataReader)
        {
            base.FillFromDataReader(entity, dataReader);
            entity.Title = dataReader[1] is DBNull ? null : Convert.ToString(dataReader[1]);
            entity.Text = dataReader[2] is DBNull ? null : Convert.ToString(dataReader[2]);
            entity.User.Id = dataReader[3] is DBNull ? 0 : Convert.ToInt32(dataReader[3]);
            entity.AdvertCategory.Id = dataReader[4] is DBNull ? 0 : Convert.ToInt32(dataReader[4]);
        }
    }
}
